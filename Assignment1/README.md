 1.Creating the maven project From Command Line:
         Run this command in terminal to create a maven project with required artifactid and groupid
 
		mvn archetype:generate -DgroupId=com.wipro.topgear -DartifactId=basicMaven -DarchetypeArtifact=maven-archetype-quickstart -DinteractiveMode=false

 2.Building the project from CMD
		Run this command to get jar file
			mvn package
			
 3.Running the main class from CMD
		run this command from CMD
			java com.wipro.topgear.App
			
 4.Running the Test Class
		run this command to run the test class from CMD
			mvn test -Dtest=AppTest