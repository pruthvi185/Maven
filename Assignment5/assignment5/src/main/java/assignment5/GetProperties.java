package assignment5;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetProperties {
	public static String get(String key)
	{
		InputStream is=GetProperties.class.getClassLoader().getResourceAsStream("myapp.properties");
		Properties p=new Properties();
		try {
			p.load(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(p.getProperty(key));
		return p.getProperty(key);
	}
}
