1. Created the maven project using the Eclipse IDE.
	while creating we used -DarchetypeArtifactId = maven-archetype-webapp  

2. Creating profiles in maven and making one of the profile as default
	by using profiles we are able to change the properities file for different environments
	
3. After that go to the folder where pom.xml contains and do 
	mvn package then it will generate war file and keeps the properties file in war depending on the profile we have selected
	
4. deploying webApp.war	
	1. First go the tomcat webapps folder and paste it
	2. go to tomcat->bin folder start tomcat by clicking startup.bat
	3. go to browser write localhost:port/project name(localhost:8013/assignment5-0.0.1-SNAPSHOT/index.jsp)
		here i am running the tomcat on 8013 port
		
MYLearning:
	1. How to create a web project using Eclipse IDE in maven
	2. Creation of profiles
	3. deploying of war file in tomcat and starting of it