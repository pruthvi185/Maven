1. Created the maven project using the Eclipse IDE.
	while creating we used -DarchetypeArtifactId = maven-archetype-webapp  
	
2. After that go to the folder where pom.xml contains and do 
	mvn package then it will generate webApp.war file
	
3. deploying webApp.war	
	1. First go the tomcat webapps folder and paste it
	2. go to tomcat->bin folder start tomcat by clicking startup.bat
	3. go to browser write localhost:port/project name(http://localhost:8013/webApp/)
		here i am running the tomcat on 8013 port
		
MYLearning:
	1. How to create a web project using Eclipse IDE in maven
	2. deploying of war file in tomcat and starting of it