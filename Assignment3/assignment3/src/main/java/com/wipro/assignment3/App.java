package com.wipro.assignment3;

import org.apache.commons.lang3.StringUtils;

import com.wipro.assignment3.dao.EmployeeDao;
import com.wipro.assignment3.model.Employee;
import com.wipro.assignment3.utility.HibernateUtility;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String[] args )
    {
       EmployeeDao dao = new EmployeeDao();
       int employeeId = dao.addEmployee("pruthvi", "b1", 35000);
       Employee emp = dao.getEmpoyeeById(employeeId);
       System.out.println("Employee Name : " + emp.getEmployeeName());
       System.out.println("Employee Salary :" + emp.getEmployeeSalary());
       System.out.println("Band :" +StringUtils.capitalize(emp.getEmployeeBand()) );
       HibernateUtility.shutDown();
    }
}
