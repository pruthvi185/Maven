package com.wipro.topgear;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
    
	@Test
	public void testLength(){
		App app = new App();
		assertEquals(5, app.length("wipro"));
	}
}
