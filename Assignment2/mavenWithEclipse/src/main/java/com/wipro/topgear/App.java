package com.wipro.topgear;

import org.apache.commons.lang3.StringUtils;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println("The length of wipro is " + new App().length("wipro") );
    }
    
    public int length(String string){
		return StringUtils.length(string);	
    }
}