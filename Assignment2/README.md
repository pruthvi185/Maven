
		1.Creating the maven project From Command Line:
         Run this command in terminal to create a maven project with required artifactid and groupid
 
		mvn archetype:generate -DgroupId=com.wipro.topgear -DartifactId=mavenWithEclipse -DarchetypeArtifact=maven-archetype-quickstart -DinteractiveMode=false

    2. Importing to Eclipse IDE
	  select import option and select import as maven project and specify the project location and this will import the project in IDE


What I learnt
	Creating maven project using cmd and importing into eclipse ide.
	How to easily download dependencies just simply by declaring them in pom.xml files
	